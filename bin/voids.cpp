#include "voids.h"

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "saturFractCommonClasses.h"
#include "workFractSaturStartFile.h"
#include "saturFractSolver.h"
#include "voids_check.h"


void do_calc_satur_fract()
{
	std::ostringstream oss;
	oss << "/home/marseille/Dropbox/self/otherWork/delika/indicator_mark/";
	oss << "global_programm/test/";
	oss << "fract_satur.imf";
						
	std::string fn = oss.str();
							
	std::shared_ptr<SaturFractFiles> srf (new SaturFractFiles());
	read_satur_start_file(fn, srf);


	std::shared_ptr<SaturFractSolver> srs (new SaturFractSolver());
	srs->initialize(srf);
	std::cout << "faces count = " << srs->grd->faces.size() << std::endl;
	std::cout << "geo faces count = " << srs->grd->glg->meshGeoFaces.size() << std::endl;
	srs->fract_index = 0;

	std::cout << "======update\n";
	srs->update(srf);
	std::cout << " srs update done\n";

	//check_fract_u(srs->grd);
	//check_res_u(srs->grd);
	//check_fract_rib_types(srs->grd);
	check_fract_rib_bound(srs->grd);
	
	double tt = srs->get_one_fract_time_step(srs->sfss->curant_number, 
			srs->fract_index);
	std::cout << "tt = " << tt << std::endl;
	std::cout << "fract_index = " << srs->fract_index << std::endl;

	return;



	double max_sat  = -1.0;
	for (auto &fcl: srs->grd->fractCells){
		int fp = srs->grd->glg->msh->meshFractCells[fcl->mesh_ind]->facePlus;
		int iof = srs->grd->glg->msh->meshFaces[fp]->indexOfFract;
		if (iof == srs->fract_index) 
			max_sat = (fcl->satur > max_sat) ? fcl->satur : max_sat;
	}
	std::cout << "max sat fr[" << srs->fract_index << "]= " << max_sat << std::endl;
}

